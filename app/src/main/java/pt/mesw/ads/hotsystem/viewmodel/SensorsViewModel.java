package pt.mesw.ads.hotsystem.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.google.firebase.database.ValueEventListener;

import java.util.List;

import pt.mesw.ads.hotsystem.engine.impl.SensorsImplementation;
import pt.mesw.ads.hotsystem.engine.impl.callback.ResponseCallback;
import pt.mesw.ads.hotsystem.model.Sensor;
import pt.mesw.ads.hotsystem.util.FirebaseUtil;
import pt.mesw.ads.hotsystem.util.SingleLiveEvent;

import static pt.mesw.ads.hotsystem.BaseConstants.SENSORS_TABLE;

public class SensorsViewModel extends AndroidViewModel {

    private ValueEventListener sensorListener;
    private MutableLiveData<List<Sensor>> sensors = new MutableLiveData<>();
    private SingleLiveEvent<Boolean> loadingAnimation = new SingleLiveEvent<>();
    private SensorsImplementation sensorsEngine = new SensorsImplementation(sensorListener);

    public SensorsViewModel(@NonNull Application application) {
        super(application);
    }

    public SingleLiveEvent<Boolean> getLoadingAnimation() {
        return loadingAnimation;
    }

    public LiveData<List<Sensor>> getSensors() {
        //TODO: check for connectivity
        loadingAnimation.setValue(true);
        sensorsEngine.setCallback(new ResponseCallback() {

            @Override
            public void onSuccess(List sensorList) {
                sensors.postValue(sensorList);
            }

            @Override
            public void onFailure() {

            }

        });

        sensorsEngine.listSensors();
        return sensors;
    }

    public void OnInteraction(){

        FirebaseUtil.getReference().child(SENSORS_TABLE).child("2").child("status").setValue(false);
    }

}
