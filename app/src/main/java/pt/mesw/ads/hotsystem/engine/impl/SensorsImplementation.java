package pt.mesw.ads.hotsystem.engine.impl;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import pt.mesw.ads.hotsystem.engine.SensorsController;
import pt.mesw.ads.hotsystem.engine.impl.callback.ResponseCallback;
import pt.mesw.ads.hotsystem.model.Sensor;
import pt.mesw.ads.hotsystem.util.FirebaseUtil;

import static android.support.constraint.Constraints.TAG;
import static pt.mesw.ads.hotsystem.BaseConstants.SENSORS_TABLE;

public class SensorsImplementation implements SensorsController {

    ValueEventListener sensorListener;
    ResponseCallback responseCallback;

    public SensorsImplementation(ValueEventListener sensorListener) {
        this.sensorListener = sensorListener;
    }

    public void setCallback(ResponseCallback responseCallback){
        this.responseCallback = responseCallback;
    }

    @Override
    public void listSensors() {

        if (sensorListener == null){
            sensorListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Get Post object and use the values to update the UI
                    ArrayList<Sensor> sensors = new ArrayList<>();
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        sensors.add(snapshot.getValue(Sensor.class));
                    }

                    Log.d("SENSOR","Sensors: " + sensors);
                    //Sensor sensorsObj =  dataSnapshot.getValue(Sensor.class);
                    //Log.d("SENSOR","Sensors: " + sensorsObj);
                    responseCallback.onSuccess(sensors);
                    // ...
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                    // ...
                    responseCallback.onFailure();
                }
            };
            FirebaseUtil.getReference().child(SENSORS_TABLE).addValueEventListener(sensorListener);
        }

    }

    @Override
    public void removeSensor(double sensorId) {

    }



}
