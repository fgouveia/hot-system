package pt.mesw.ads.hotsystem.model;

import java.util.List;

public class Sensor {

    private double id;
    private String name;
    private double value;
    private String unit;
    private boolean interactable;
    private boolean status;
    private List<String> statusName;

    public Sensor(){}

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isInteractable() {
        return interactable;
    }

    public void setInteractable(boolean interactable) {
        this.interactable = interactable;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<String> getStatusName() {
        return statusName;
    }

    public void setStatusName(List<String> statusName) {
        this.statusName = statusName;
    }


}
