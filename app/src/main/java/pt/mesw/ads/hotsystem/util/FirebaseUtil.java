package pt.mesw.ads.hotsystem.util;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUtil {

    public static String PREF = "kirb.com.br.kirb.PREF";

    private static DatabaseReference firebaseReference;

    private FirebaseUtil(){}

    public static DatabaseReference getReference(){
        if( firebaseReference == null ){
            firebaseReference = FirebaseDatabase.getInstance().getReference();
        }

        return( firebaseReference );
    }

    public static DatabaseReference getRefUrl(String url){
        if( firebaseReference == null ){
            firebaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl(url);
        }

        return( firebaseReference );
    }

}
