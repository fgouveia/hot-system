package pt.mesw.ads.hotsystem.util;

import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;

public class Converter {

    public static double convertDPToPixels(int dp, FragmentActivity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;
        return dp * logicalDensity * 0.67;
    }
}
