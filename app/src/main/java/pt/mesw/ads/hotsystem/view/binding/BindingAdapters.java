package pt.mesw.ads.hotsystem.view.binding;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class BindingAdapters {

    /**
     * Receives a boolean value and changes visibility of the view based on the boolean value
     * @param view is the view
     * @param bool is the boolean value
     */
    @BindingAdapter("setVisibility")
    public static void setVisibility(View view, boolean bool) {
        view.setVisibility(bool ? View.VISIBLE : View.GONE);
    }


    /**
     * Receives a boolean value and changes visibility of the view based on the boolean value
     * @param textView is the view
     * @param status is the boolean value
     */
    @BindingAdapter({"setStatus", "setValue", "setUnit", "setStatusName", "setInteractable"})
    public static void setText(TextView textView, boolean status, double value, String unit, List<String> statusName, boolean interactable) {
        if(interactable){
            if(status) {
                textView.setText(statusName.get(0));
            } else {
                textView.setText(statusName.get(1));
            }
        } else {
                textView.setText(String.valueOf(value) + unit);
        }
    }

}
