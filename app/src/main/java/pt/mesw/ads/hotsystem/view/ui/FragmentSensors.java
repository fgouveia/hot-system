package pt.mesw.ads.hotsystem.view.ui;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import java.util.Objects;

import pt.mesw.ads.hotsystem.BaseConstants;
import pt.mesw.ads.hotsystem.R;
import pt.mesw.ads.hotsystem.databinding.FragmentSensorsBinding;

import pt.mesw.ads.hotsystem.util.Converter;
import pt.mesw.ads.hotsystem.view.SensorClickCallback;
import pt.mesw.ads.hotsystem.view.adapter.SensorsAdapter;
import pt.mesw.ads.hotsystem.viewmodel.SensorsViewModel;

public class FragmentSensors extends Fragment {

    private SensorClickCallback activityCallback;
    private FragmentSensorsBinding binding;
    private SensorsViewModel viewModel;

    private SensorClickCallback clickCallback = sensor -> activityCallback.onClick(sensor);


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(SensorsViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sensors, container, false);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        configRecyclerView();
        SensorsAdapter adapter = new SensorsAdapter(clickCallback);
        viewModel.getSensors().observe(this, sensorList -> adapter.setSensorList(sensorList));
        binding.recyclerSensors.setAdapter(adapter);
        return binding.getRoot();
    }

    private void configRecyclerView() {
        binding.recyclerSensors.setLayoutManager(new GridLayoutManager(getActivity(), 4, LinearLayoutManager.VERTICAL, false));
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        binding.recyclerSensors.setLayoutAnimation(animation);
    }

    /* Makes sure we are implementing On*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            clickCallback = (SensorClickCallback) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement SensorClickCallback");
        }
    }

    private int calculateSpanCount() {
        int spanCount = (int) Math.floor(binding.recyclerSensors.getWidth() / Converter.convertDPToPixels(BaseConstants.MOVIE_ITEM_WIDTH, Objects.requireNonNull(getActivity())));
        ((GridLayoutManager) Objects.requireNonNull(binding.recyclerSensors.getLayoutManager())).setSpanCount(spanCount);
        return spanCount;
    }

}
