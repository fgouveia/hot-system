package pt.mesw.ads.hotsystem.engine;

public interface SensorsController {

    void listSensors();
    void removeSensor(double sensorId);

}
