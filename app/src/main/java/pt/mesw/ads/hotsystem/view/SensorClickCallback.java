package pt.mesw.ads.hotsystem.view;

import pt.mesw.ads.hotsystem.model.Sensor;

public interface SensorClickCallback {

    void onClick(Sensor sensor);
}
