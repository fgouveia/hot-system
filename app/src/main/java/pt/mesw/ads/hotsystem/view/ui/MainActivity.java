package pt.mesw.ads.hotsystem.view.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pt.mesw.ads.hotsystem.R;
import pt.mesw.ads.hotsystem.model.Sensor;
import pt.mesw.ads.hotsystem.util.Navigator;
import pt.mesw.ads.hotsystem.view.SensorClickCallback;

public class MainActivity extends AppCompatActivity implements SensorClickCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null){
            Navigator.addFragment(R.id.sensors_container, new FragmentSensors(), "FragmentSensors", this);
            Navigator.addFragment(R.id.summary_container, new FragmentSummary(), "FragmentSummary", this);
        }
    }

    @Override
    public void onClick(Sensor sensor) {
        //TODO: Implement detail view
    }
}
