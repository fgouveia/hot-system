package pt.mesw.ads.hotsystem;

public class BaseConstants {

    public static final String SENSORS_TABLE = "sensors";
    public static final int MOVIE_ITEM_WIDTH = 150;
}
