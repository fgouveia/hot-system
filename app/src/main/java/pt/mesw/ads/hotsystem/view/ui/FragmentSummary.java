package pt.mesw.ads.hotsystem.view.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.mesw.ads.hotsystem.R;
import pt.mesw.ads.hotsystem.databinding.FragmentSummaryBinding;

public class FragmentSummary extends Fragment {

    private FragmentSummaryBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_summary, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }
}
