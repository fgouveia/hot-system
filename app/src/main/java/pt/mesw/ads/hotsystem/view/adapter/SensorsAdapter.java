package pt.mesw.ads.hotsystem.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import pt.mesw.ads.hotsystem.R;
import pt.mesw.ads.hotsystem.databinding.ItemSensorBinding;
import pt.mesw.ads.hotsystem.model.Sensor;
import pt.mesw.ads.hotsystem.view.SensorClickCallback;

public class SensorsAdapter extends RecyclerView.Adapter<SensorsAdapter.SensorViewHolder> {

    @Nullable
    private final SensorClickCallback sensorClickCallback;

    private List<? extends Sensor> sensorList;

    public SensorsAdapter(@Nullable SensorClickCallback sensorClickCallback) {
        this.sensorClickCallback = sensorClickCallback;
    }

    public void setSensorList(final List<? extends Sensor> sensors) {
        if (sensorList == null) {
            sensorList = sensors;
            notifyItemRangeInserted(0, sensors.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return sensorList.size();
                }

                @Override
                public int getNewListSize() {
                    return sensors.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    Sensor oldSensor = sensorList.get(oldItemPosition);
                    Sensor newSensor = sensors.get(newItemPosition);
                    return oldSensor.getId() == newSensor.getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Sensor oldSensor = sensorList.get(oldItemPosition);
                    Sensor newSensor = sensors.get(newItemPosition);
                    return oldSensor.getName() == newSensor.getName()
                            && oldSensor.getValue() == newSensor.getValue()
                            && oldSensor.getUnit() == newSensor.getUnit()
                            && oldSensor.isStatus() == newSensor.isStatus()
                            && oldSensor.isInteractable() == newSensor.isInteractable();
                }
            });
            sensorList = sensors;
            diffResult.dispatchUpdatesTo(this);
        }
    }


    @NonNull
    @Override
    public SensorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSensorBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_sensor,
                        parent, false);
        binding.setCallback(sensorClickCallback);
        return new SensorViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SensorViewHolder holder, int position) {
        holder.binding.setSensor(sensorList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return sensorList == null ? 0 : sensorList.size();
    }

    static class SensorViewHolder extends RecyclerView.ViewHolder {

        final ItemSensorBinding binding;

        SensorViewHolder(ItemSensorBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
