package pt.mesw.ads.hotsystem.engine.impl.callback;

import java.util.List;

public interface ResponseCallback<T> {

    void onSuccess(List<T> sensorList);
    void onFailure();
}
